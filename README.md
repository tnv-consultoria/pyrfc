# PyRFC : How to **REALLY** install this thing

## Description

This project is a wrapper for [SAP NetWeaver RFC SDK](https://support.sap.com/en/product/connectors/nwrfcsdk.html) for Python.

While nwrfcsdk is available for Solaris, MacOS, Linux, Windows, AIX (!) and HP-UX (!!!), only Linux and Windows targets were defined on the PyRFC.

This project aims to fix this.

## Targets

### MacOS

You don't need to install nwrfcsdk on the `/usr/` filespace. With little to no effort, you can install it on the more appropriate `/usr/local` or `/opt/`, but the best place will be `~/.local` (on the user's home directory", what would allow fine control on what programs can access it - and even install more than one completelty different version (on a different user) to run on the same machine.

#### Pass 1: Install SAP nwrfcsdk

Download nwrfc750P_8-80002761 (see links below) and unzip the subdir nwrfcsdk into `~/.local` (create it if it doesn't exists). You can ignore the META-INF subdir.

Edit your `~/.profile` file, appending to it what follows:

```
## SAP NW RFC SDK 7.50
export DYLD_FALLBACK_LIBRARY_PATH="/${HOME}/.local/nwrfcsdk/lib"
export SAPNWRFC_HOME=~/.local/nwrfcsdk
export PATH=$PATH:${SAPNWRFC_HOME}/bin
launchctl setenv DYLD_FALLBACK_LIBRARY_PATH $DYLD_FALLBACK_LIBRARY_PATH
launchctl setenv SAPNWRFC_HOME $SAPNWRFC_HOME
```

#### Pass 2: Instal PyRFC

Read the original [instructions](https://sap.github.io/PyRFC/build.html#build), but forking **this** project instead and then:

```
sudo port install gcc6
sudo port select gcc mp-gcc6
export CC=/opt/local/bin/gcc
python setup.py bdist_egg
```


## References

* Original [README](./README.PyRFC.md).
* [Installation and Availability](https://launchpad.support.sap.com/#/notes/2573790).
	* [Link for download](https://launchpad.support.sap.com/#/softwarecenter/template/products/_APP=00200682500000001943&_EVENT=DISPHIER&HEADER=Y&FUNCTIONBAR=N&EVENT=TREE&NE=NAVIGATE&ENR=01200314690100002214&V=MAINT) (select the target platform!)
* [How to Compile and Link RFC programs](https://launchpad.support.sap.com/#/notes/2573953) (for C and C++ programs)
* PyRFC:
	* [Installation](https://sap.github.io/PyRFC/install.html) 
	* [Building from Sources](https://sap.github.io/PyRFC/build.htm)
	* Original [Source on GitHub]( https://github.com/SAP/PyRFC).